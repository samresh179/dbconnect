FROM openjdk:8-jdk-alpine
COPY target/sampleCRUDProject-0.0.1-SNAPSHOT.jar sampleCRUDProject-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/sampleCRUDProject-0.0.1-SNAPSHOT.jar"]