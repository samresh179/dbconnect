package com.samplecrud.utils;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.cloud.secretmanager.v1.AccessSecretVersionResponse;
import com.google.cloud.secretmanager.v1.SecretManagerServiceClient;
import com.google.cloud.secretmanager.v1.SecretVersionName;


@Service
public class SmUtils {

	@Value("${spring.application.project-id}")
	public String projectId;

	private static final String LATEST = "latest";
	public String accessSecretVersion(String projectId, String secretId, String versionId) throws IOException {
		try (SecretManagerServiceClient client = SecretManagerServiceClient.create()) {
			SecretVersionName secretVersionName = SecretVersionName.of(projectId, secretId, versionId);
			AccessSecretVersionResponse response = client.accessSecretVersion(secretVersionName);
			return response.getPayload().getData().toStringUtf8();
		}
	}

	public String accessSecretVersion(String secretId) throws IOException {
		return accessSecretVersion(this.projectId, secretId, LATEST);

	}

}
