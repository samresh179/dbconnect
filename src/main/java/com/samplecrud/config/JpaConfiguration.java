
package com.samplecrud.config;

import java.io.IOException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.samplecrud.utils.SmUtils;

@Configuration
public class JpaConfiguration {

	@Value("${spring.jpa.driverClassName}")
	String driverClassName;

	@Value("${spring.application.project-id}")
	String projectId;

	@Autowired
	SmUtils smUtils;

	private static final String SPRING_DATASOURCE_URL = "spring-datasource-url";
	private static final String SPRING_DATASOURCE_USER_NAME = "spring-datasource-username";
	private static final String SPRING_DATASOURCE_PASSWORD = "spring-datasource-password";
	public static final String ENTITYMANGER = "entityManager";
	private static final String DATASOURCE = "dataSource";

	@Primary

	@Bean(name = DATASOURCE)

	@ConfigurationProperties(prefix = "spring.jpa")
	public DataSource dataSource() {
		DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
		dataSourceBuilder.driverClassName(driverClassName);
		try { //
			dataSourceBuilder.url(smUtils.accessSecretVersion(projectId, SPRING_DATASOURCE_URL, "1"));
			dataSourceBuilder.url(smUtils.accessSecretVersion(SPRING_DATASOURCE_URL));
			dataSourceBuilder.username(smUtils.accessSecretVersion(SPRING_DATASOURCE_USER_NAME));
			dataSourceBuilder.password(smUtils.accessSecretVersion(SPRING_DATASOURCE_PASSWORD));

		} catch (IOException e) {
			e.printStackTrace();
		}
		return dataSourceBuilder.build();
	}

}
