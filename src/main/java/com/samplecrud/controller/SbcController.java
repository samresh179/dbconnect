package com.samplecrud.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.samplecrud.exception.CustomerNotFoundException;
import com.samplecrud.model.customer;
import com.samplecrud.service.SbcService;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping("/crudapp")
public class SbcController {
	
	Logger log = LoggerFactory.getLogger(SbcController.class);
	
	@Autowired
	SbcService custService;
	
	
	@GetMapping("/samplecustdata")
	public String getCustData()
	{
		log.info("Inside controller class");
		return "SbcController is called to return sample Customer Data::";
	}
	
	@Operation(summary = "Retrieve All Customers", description = "Method used to retrieve all the customers")
	@GetMapping("/custdata")
	public List<customer> getAllCustomers(){
		return custService.getAllCustomers();		
	}
	
	@Operation(summary = "Retrieve the Customer by Id", description = "Method used to retrieve the customer by id")
	@GetMapping("/custdata/{id}")
	public customer getCustomerById(@PathVariable("id") long id) throws CustomerNotFoundException{
		return custService.getCustomerById(id) ;
	}
	
	@Operation(summary = "Create Customer", description = "Method used to create a customer")
	@PostMapping("/custdata")
	public customer createCustomer(@RequestBody customer customer){
		System.out.println("values in Sbc controller::"+customer);
		return custService.createCustomer(customer);
		
	}
	
	@ExceptionHandler(CustomerNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ResponseEntity<String> handleCustomerNotFoundException(CustomerNotFoundException excep){
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(excep.getMessage());
		
	}
	

}
