package com.samplecrud.serviceImpl;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.samplecrud.exception.CustomerNotFoundException;
import com.samplecrud.model.customer;
import com.samplecrud.repository.SbcRepository;
import com.samplecrud.service.SbcService;

@Service
public class SbcServiceImpl implements SbcService{

	Logger log = LoggerFactory.getLogger(SbcServiceImpl.class);
	
	@Autowired
	private SbcRepository custRepo;

	@Override
	public List<customer> getAllCustomers() {
		List<customer> customers=new ArrayList<>();
		custRepo.findAll().forEach(customer -> customers.add(customer));
        return customers;
	}

	@Override
	public customer getCustomerById(long custId)  {
		return custRepo.findById(custId).orElseThrow(() -> new CustomerNotFoundException("Customer not found with id "+ custId));

	}
	
	@Override
	public List<customer> getCustomerByFirstName(String firstName)
	{
		return custRepo.findByFirstnameStarsWith(firstName);
		
	}

	@Override
	public customer createCustomer(customer customer) {
		log.info("values::"+customer.toString());
		customer.setDate(new Date());
		return custRepo.save(customer);

	}
	
	
}
