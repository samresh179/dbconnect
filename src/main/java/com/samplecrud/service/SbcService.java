package com.samplecrud.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.samplecrud.model.customer;

@Service
public interface SbcService {
	
	Logger log = LoggerFactory.getLogger(SbcService.class);
	
	public List<customer> getAllCustomers();	
	public customer getCustomerById(long custId);
	public customer createCustomer(customer customer);
	public List<customer> getCustomerByFirstName(String firstName);

}
