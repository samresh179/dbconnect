package com.samplecrud.repository;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.samplecrud.model.customer;

@Repository
public interface SbcRepository  extends CrudRepository<customer, Long> {
	
	@Query("select u from customer u where u.firstName = ?1")
	List<customer> findByFirstnameStarsWith(String firstName);

}
